import bpy
import urllib
import json
import zipfile
import os
import math
import bmesh
from bpy.types import Operator

from . import utils

from bpy.props import (
        StringProperty,
        BoolProperty,
        IntProperty,
        FloatProperty,
        FloatVectorProperty,
        EnumProperty,
        )


class op_create_sticky_material(Operator):
    bl_idname = "gis.create_sticky_material"
    bl_label = "Create Sticky Material"
    bl_description = "Creates a material which automatically aligns with the basemap"
    bl_options = {'REGISTER', 'UNDO'}
    

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=250)

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text="Create sticky material?", icon='MATERIAL')

    def execute(self, context):
        if not utils.is_scene_georef():
            self.report({'ERROR'}, "No basemap...\n")
            return {'CANCELLED'}
        else:
            utils.ensure_basemap_scale()
            basemap = utils.get_basemap_obj()
            mat_name = "Sticky " + basemap.name
            # Test if material exists
            # If it does not exist, create it:
            mat = (bpy.data.materials.get(mat_name) or 
                bpy.data.materials.new(mat_name))

            # Enable 'Use nodes':
            mat.use_nodes = True
            nodes = mat.node_tree.nodes
            links = mat.node_tree.links

            nodes.clear()

            # Add a diffuse shader and set its location:    
            n_diffuse = nodes.new('ShaderNodeBsdfDiffuse')
            n_diffuse.location = (100,0)

            n_output = nodes.new('ShaderNodeOutputMaterial')
            n_output.location = (300,0)

            n_geo = nodes.new('ShaderNodeNewGeometry')
            n_geo.location = (-1000,0)

            n_vmath_offset =  nodes.new('ShaderNodeVectorMath')
            n_vmath_offset.location = (-800,0)
            n_vmath_offset.operation = 'SUBTRACT'
            n_vmath_offset.label = 'Offset'
            
            n_vmath_offset.inputs[1].default_value[0] = basemap.location.x
            n_vmath_offset.inputs[1].default_value[1] = basemap.location.y
            
            n_separate =  nodes.new('ShaderNodeSeparateXYZ')
            n_separate.location = (-800,-300)
            
            n_vmath_add =  nodes.new('ShaderNodeVectorMath')
            n_vmath_add.location = (-600,0)
            n_vmath_add.operation = 'ADD'
            
            n_vmath_perspective =  nodes.new('ShaderNodeVectorMath')
            n_vmath_perspective.location = (-600,-300)
            n_vmath_perspective.operation = 'MULTIPLY'
            n_vmath_perspective.label = 'Perspective Correct'
            


            n_mapping = nodes.new('ShaderNodeMapping')
            n_mapping.location = (-400,0)
            
            sscale = utils.get_scene_scale_m()
            
            n_mapping.inputs[1].default_value[0] = 0.5
            n_mapping.inputs[1].default_value[1] = 0.5

            n_mapping.inputs[3].default_value[0] = 1 / sscale[0]
            n_mapping.inputs[3].default_value[1] = 1 / sscale[1]

            n_tex = nodes.new('ShaderNodeTexImage')
            n_tex.location = (-200,0)
            n_tex.image = basemap.data
            n_tex.extension = 'EXTEND'

            links.new(n_vmath_offset.inputs[0], n_geo.outputs[0])
            links.new(n_separate.inputs[0], n_geo.outputs[0])
            links.new(n_vmath_perspective.inputs[0], n_separate.outputs[2])
            links.new(n_vmath_add.inputs[0], n_vmath_offset.outputs[0])
            links.new(n_vmath_add.inputs[1], n_vmath_perspective.outputs[0])
            links.new(n_mapping.inputs[0], n_vmath_add.outputs[0])
            links.new(n_tex.inputs[0], n_mapping.outputs[0])
            links.new(n_diffuse.inputs[0], n_tex.outputs[0])
            links.new(n_output.inputs[0], n_diffuse.outputs[0])
    
            self.report({'INFO'}, "New sticky material created with name: '" + mat_name + "'" )
            return {'FINISHED'}


class op_get_coordinate_info(Operator):
    bl_idname = "gis.get_coordinate_info"
    bl_label = "Get Coordinate Info"
    bl_description = "A modal window that provides general info about the coordinates"
    bl_options = {'REGISTER', 'UNDO'}
    
    str_lon: StringProperty(
        name = "lon",
        default = "default"
    )
    
    str_lat: StringProperty(
        name = "lat",
        default = "default"
    )
    
    str_rd_x: StringProperty(
        name = "RD X",
        default = "default"
    )
    
    str_rd_y: StringProperty(
        name = "RD Y",
        default = "default"
    )
    
    str_size_x: StringProperty(
        name = "Size X",
        default = "default"
    )
    
    str_size_y: StringProperty(
        name = "Size Y",
        default = "default"
    )
    srt_size_mercator: StringProperty(
        name = "Scale relative to Web Mercator",
        default = "default"
    )

    
    
    

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=250)

    def draw(self, context):
        if not utils.is_scene_georef():
            row.label(text="No basemap found")
        else:            
            layout = self.layout
            row = layout.row()
            row.label(text="Centerpoint Lon/lat", icon='SPHERE')
                        
            lon = bpy.context.scene["longitude"]
            lat = bpy.context.scene["latitude"]     
            
            self.str_lon = str(lon)
            self.str_lat = str(lat)
            self.srt_size_mercator = str(utils.get_mercator_scale())
            
            row = layout.row()
            row.prop(self, "str_lon")
            row = layout.row()
            row.prop(self, "str_lat")
            row = layout.row()
            row.prop(self, "srt_size_mercator")
            
            row = layout.row()
            row.label(text="Centerpoint Amersfoort / RD New", icon='MESH_GRID')
            
            rd = utils.Rijksdriehoek()
            rd.from_wgs(lat,lon)
            
            self.str_rd_x = str(rd.rd_x)
            self.str_rd_y = str(rd.rd_y)
            
            row = layout.row()
            row.prop(self, "str_rd_x")
            row = layout.row()
            row.prop(self, "str_rd_y")
            
            size = utils.get_scene_scale_m()
            empty_scale = utils.get_basemap_obj().empty_display_size
            size_multiply = 1
            
            if(empty_scale == 1):
                size_multiply = utils.get_mercator_scale()
                print("Empty is still full size, scale: " + str(size_multiply))
            
            
            self.str_size_x = str(size[0] * size_multiply)
            self.str_size_y = str(size[1] * size_multiply)
            
            row = layout.row()
            row.label(text="Scene Size in Meters", icon='FIXED_SIZE')

            row = layout.row()
            row.prop(self, "str_size_x")
            row = layout.row()
            row.prop(self, "str_size_y")
            
            

    def execute(self, context):
        return {'FINISHED'}
    
