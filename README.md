# Blender Hoogtedata Addon
_Use Dutch public height data inside Blender_\
_**Warning**: Due to API changes, I can't garuantee continued operation._\
_If you know a little python, pullrequests for improvements or fixes are much appreciated!_


| AHN3 DSM | AHN3 DTM | 3D BAG |
| - | - | - |
| ![](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/uploads/1eaa2b98845de9c767414549b9ccf5f6/DSM.png) | ![](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/uploads/31722e294345df88cdff31eaf8088a0d/DTM.png) | ![](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/uploads/dfcb60ece6618717f006e756738db6eb/BAG.png) |

### [💾 DOWNLOAD](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/archive/main/blender-hoogtedata-addon-main.zip) - [📒 HOW-TO](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/home) - [🧵 BLENDERARTISTS THREAD](https://blenderartists.org/t/blender-hoogtedata-addon-dutch-height-data-in-blender/1371153)

![](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/uploads/5b44c8ddf021eba6704fd13d6f2b0312/image.png)

## Overview

This Addon for [Blender](http://www.blender.org/) allows you to use public 3D information, such as [AHN (height data)](http://www.ahn.nl) and [BAG3D](http://www.3dbag.nl).\
It is meant to be used in combination with a [BlenderGIS](https://github.com/domlysz/BlenderGIS/) basemap.

## Installation

This addon depends on [_BlenderGIS_](https://github.com/domlysz/BlenderGIS/), which can be found [here](https://github.com/domlysz/BlenderGIS/)

To install the _Blender Hoogtedata Addon_, download the following ZIP file:\
https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/archive/main/blender-hoogtedata-addon-main.zip \
Then install using `Blender > Edit > Preferences > Add-ons > Install... `


## Usage

### [_Using Blender Hoogtedata Addon_](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/home)

In short:

- **Fetch a Basemap using BlenderGIS.**
    - read more on [how to fetch a basemap](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/Basemap)
- **Use this addon to fetch height data to match the basemap.**
    - A mesh is automatically created
    - You can chose between ground-level height or height with buildings and vegetation
    - Read more about [how to use AHN heightmaps](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/AHN)
- **Automatically download 3D BAG buildings to match the basemap.**
    - Choose between different levels of detail
    - Read more about [using 3D BAG buildings](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/3D-Bag)



![](https://gitlab.com/thomaskole/blender-hoogtedata-addon/-/wikis/uploads/f5b4428192739bdba1402c7cc8321a4f/maastricht.jpg)
